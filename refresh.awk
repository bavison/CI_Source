#!/usr/bin/awk -f

# Subroutine to parse the components file.
function find_project_builds(build_class) {
  for (i in builds[build_class]) {
    build = builds[build_class][i]
    cmd = "sed -e 's/[#%].*//' BuildSys/Components/ROOL/" build
    while ((cmd | getline) > 0) {
      if (NF != 0) {
        # Use dummy value to cope with a component appearing more than once
        projects[components[$1]["project"]]["builds"][build_class][build] = "yes"
        projects[components[$1]["project"]]["buildable"][build_class] = "yes"
      }
    }
    close(cmd)
  }
}

# Subroutine to insert rules to permit a job to be suppressed.
function check_suppress(job) {
  print "  rules:"                                      >> out
  print "    - if: '$SUPPRESS_JOBS !~ /\\b" job "\\b/'" >> out
}

# Subroutine to insert rules to permit a job to be enabled.
function check_enable(job) {
  print "  rules:"                                      >> out
  print "    - if: '$ENABLE_JOBS =~ /\\b" job "\\b/'"   >> out
}

# Subroutine to write softload rules for a submodule project.
function write_softload_rules(toolchain) {
  print "softload" toolchain ":"         >> out
  print "  stage: softload"              >> out
  print "  tags: [ cross ]"              >> out
  if (toolchain == "_gnu")
    check_enable("softload_gnu")
  else
    check_suppress("softload" toolchain)
  if (toolchain == "_gnu") {
    print "  variables:"                 >> out
    print "    TOOLCHAIN: GNU"           >> out
  }
  print "  script:"                      >> out
  print "    - 'curl --location --output ~/cache/common/Disc.zip -z ~/cache/common/Disc.zip \"https://gitlab.riscosopen.org/Products/Disc/-/jobs/artifacts/master/download?job=latest_package_tree\"'" >> out
  print "    - unzip -q ~/cache/common/Disc.zip && rm -rf RiscOS/Install"                      >> out
  print "    - '[ ! -e BuildInfo ] || cat BuildInfo'"                                          >> out
  print "    - source RiscOS/Env/ROOL/Disc.sh"                                                 >> out
  for (i = 0; i < projects[project]["ncomponents"]; ++i) {
    component = projects[project]["component"][i]
    if (components[component]["path"] == projects[project]["path"])
      rel = ""
    else
      rel = " -C " substr(components[component]["path"], length(projects[project]["path"]) + 2)
    print "    - COMPONENT=" component " TARGET=" components[component]["target"] " INSTDIR=$INSTALLDIR make -k" rel " clean"               >> out
    print "    - COMPONENT=" component " TARGET=" components[component]["target"] " INSTDIR=$INSTALLDIR make -k" rel " export_hdrs || true" >> out
    print "    - COMPONENT=" component " TARGET=" components[component]["target"] " INSTDIR=$INSTALLDIR make -k" rel " export_libs || true" >> out
    print "    - COMPONENT=" component " TARGET=" components[component]["target"] " INSTDIR=$INSTALLDIR make -k" rel " install"             >> out
  }
  print "  artifacts:"                   >> out
  print "    paths:"                     >> out
  print "      - RiscOS/Install/*"       >> out
  print "  # For now, we expect some failed components" >> out
  print "  allow_failure: true"          >> out
  print ""                               >> out
}

# Subroutine to write build rules for a submodule project.
function write_build_rules(build_class) {
  backlink = gensub(/[^/]*/, "..", "g", projects[project]["path"])
  backlink = gensub(/..\//, "", "1", backlink)
  if (projects[project]["buildable"][build_class] == "yes") {
    for (build in projects[project]["builds"][build_class]) {
      if (build == "IOMD32")
        superproject = "IOMDHAL"
      else
        superproject = build
      print build_class "_" build ":"        >> out
      print "  stage: " build_class          >> out
      print "  tags: [ cross ]"              >> out
      check_suppress(build_class "_" build)
      print "  script:"                      >> out
      print "    - 'curl --location --output ~/cache/common/" build ".zip -z ~/cache/common/" build ".zip \"https://gitlab.riscosopen.org/Products/" superproject "/-/jobs/artifacts/master/download?job=latest_package_tree\"'" >> out
      print "    - unzip -q ~/cache/common/" build ".zip && mkdir -p " projects[project]["path"] " && rm -rf " projects[project]["path"] " && ln -s " backlink " " projects[project]["path"]                                   >> out
      print "    - source RiscOS/Env/ROOL/" build ".sh"                                                 >> out
      print "    # Run all " build_class " build phases, stopping at the first one that fails (if any)" >> out
      print "    - srcbuild export_hdrs"     >> out
      print "    - srcbuild export_libs"     >> out
      if (build_class == "disc") {
        print "    - srcbuild install"       >> out
        print "  artifacts:"                 >> out
        print "    paths:"                   >> out
        print "      - RiscOS/Install/*"     >> out
      } else {
        print "    - srcbuild resources"     >> out
        print "    - srcbuild rom"           >> out
        print "    - srcbuild install_rom"   >> out
        print "    - srcbuild join"          >> out
        print "  artifacts:"                 >> out
        print "    paths:"                   >> out
        print "      - RiscOS/Images/b*"     >> out
      }
      print "  # For now, we expect some failed components" >> out
      print "  allow_failure: true"          >> out
      print ""                               >> out
    }
  }
}

# Subroutine to write complete YAML file for a superproject.
function do_superproject_class(build_class) {
  if (build_class == "disc")
    build_phase_list = "export_hdrs export_libs"
    # eventually expand this to "export_hdrs export_libs install"
  else # build_class == "rom"
    build_phase_list = "export_hdrs export_libs"
    # eventually expand this to "export_hdrs export_libs resources rom install_rom join"
  split(build_phase_list, phases)
  for (b in builds[build_class]) {
    if (builds[build_class][b] == "IOMD32")
      out = "CI/IOMDHAL.yml"
    else
      out = "CI/" builds[build_class][b] ".yml"

    print "workflow:"                       >> out
    print "  rules:"                        >> out
    print "    - if: $CI_MERGE_REQUEST_IID" >> out
    print "    - if: $CI_COMMIT_TAG"        >> out
    print "    - if: $CI_COMMIT_BRANCH"     >> out
    print ""                                >> out

    # When we get to a point where we're doing a release (i.e. pushing commits
    # to the superproject that contain a meaningful combination of submodule
    # commit hashes) then it would give developers faster feedback if the
    # 'snapshot' jobs are done first. The 'latest' jobs are there to update
    # the artifacts used in each individual component's CI jobs, and will be run
    # overnight on a schedule, so their speed is less critical.
    print "stages:"                       >> out
    print "  - snapshot_fetch"            >> out
    for (phase in phases) {
      print "  - snapshot_" phases[phase] >> out
    }
    print "  - snapshot_package"          >> out
    print "  - latest_fetch"              >> out
    for (phase in phases) {
      print "  - latest_" phases[phase]   >> out
    }
    print "  - latest_package"            >> out
    print "  - cleanup"                   >> out
    print ""                              >> out

    print "# For most jobs, we don't want the Runner to do any git operations" >> out
    print "variables:"                    >> out
    print "  GIT_STRATEGY: none"          >> out
    print ""                              >> out
    print "# Other default settings"      >> out
    print "default:"                      >> out
    print "  before_script:"              >> out
    print "    - source RiscOS/Env/ROOL/" builds[build_class][b] ".sh" >> out
    print ""                              >> out

    split("snapshot latest", types)
    for (t in types) {
      print types[t] "_fetch:"            >> out
      print "  stage: " types[t] "_fetch" >> out
      print "  tags: [ cross ]"           >> out
      print "  variables:"                >> out
      print "    GIT_STRATEGY: fetch"     >> out
      print "  before_script:"            >> out
      print "    # Runner only cleans superproject by default - explicitly clean submodules" >> out
      print "    - git submodule foreach 'git clean -xdf && git checkout -f'" >> out
      if (types[t] == "snapshot") {
        print "    - git submodule foreach 'git fetch origin'"      >> out
        print "    - git submodule update --init --jobs 8"          >> out
      }
      else { # types[t] == "latest"
        print "    - git submodule update"                          >> out
        print "    - git submodule update --remote --no-fetch"      >> out
      }
      print "    # Tweaks are required to several components to permit cross-compilation, but are currently stuck in review."                                     >> out
      print "    # Pull them into the source tree explicitly (note that this assumes they are all in bavison's fork projects, which is true at time of writing)." >> out
      print "    # Eventually we should be able to remove this line entirely, which will speed up builds a lot."                                                  >> out
      print "    # Build a \"Manifest\" file at the top level while we do this."                                                                                  >> out
      print "    - rm -f Manifest Pending Diverged"                                                                                                               >> out
      print "    - >"                                                                                                                                             >> out
      print "        git submodule foreach '"                                                                                                                     >> out
      print "        oldrev=$(git rev-parse HEAD);"                                                                                                               >> out
      print "        git remote remove bavison || :;"                                                                                                             >> out
      print "        git remote add -f bavison https://gitlab.riscosopen.org/bavison/$(basename $(pwd)).git;"                                                     >> out
      print "        git branch -D CrossCompilationSupport || :;"                                                                                                 >> out
      print "        ( git checkout CrossCompilationSupport &&"                                                                                                   >> out
      print "        ( echo \"$sm_path\" >> $toplevel/Pending;"                                                                                                   >> out
      print "        [ $oldrev = $(git merge-base $oldrev HEAD) ] ||"                                                                                             >> out
      print "        echo \"$sm_path\" >> $toplevel/Diverged ) ) || :;"                                                                                           >> out
      print "        ( echo -n \"$sm_path: \";"                                                                                                                   >> out
      print "        git describe --abbrev=8 --tags --always ) >> $toplevel/Manifest;'"                                                                           >> out
      print "    - echo Using " types[t] " build tree generated with this pipeline > BuildInfo"                                                                   >> out
      print "    - echo $CI_PIPELINE_URL >> BuildInfo"                                                                                                            >> out
      print "    - echo Search for \\'cat Manifest\\' in $CI_JOB_NAME job log for versions >> BuildInfo"                                                          >> out
      print "    - echo $CI_JOB_URL >> BuildInfo"                                                                                                                 >> out
      print "    - '# Fetched the following versions of each submodule.'"                                                                                         >> out
      print "    - cat Manifest"                                                                                                                                  >> out
      print "    - '# Using CrossCompilation branch for the following submodules.'"                                                                               >> out
      print "    - '# If you rely on any of them in your builds, remember that they are subject'"                                                                 >> out
      print "    - '# to change during review, so consider marking your submission as dependent'"                                                                 >> out
      print "    - '# upon the relevant MR.'"                                                                                                                     >> out
      print "    - cat Pending"                                                                                                                                   >> out
      print "    - '# For the following submodules, mainline development has diverged from the'"                                                                  >> out
      print "    - '# CrossCompilationSupport branch. If you require features from mainline since'"                                                               >> out
      print "    - '# this point, nag Ben to rebase the CrossCompilationSupport branch.'"                                                                         >> out
      print "    - cat Diverged"                                                                                                                                  >> out
      print "  script: echo"              >> out
      print "  dependencies: []"          >> out
      print ""                            >> out
      for (p in phases) {
        print types[t] "_" phases[p] ":"         >> out
        print "  stage: " types[t] "_" phases[p] >> out
        print "  tags: [ cross ]"                >> out
        print "  script: srcbuild " phases[p]    >> out
        print "  dependencies: []"               >> out
        print "  # For now, we expect some failed components" >> out
        print "  allow_failure: true"            >> out
        print ""                                 >> out
      }
      if (build_class == "disc") {
        print types[t] "_package_disc:"          >> out
        print "  stage: " types[t]"_package"     >> out
        print "  tags: [ cross ]"                >> out
        print "  before_script: []"              >> out
        print "  script: echo"                   >> out
        print "  dependencies: []"               >> out
        print "  artifacts:"                     >> out
        print "    paths:"                       >> out
        print "      - RiscOS/Install/*"         >> out
        print ""                                 >> out
      } else { # build_class == "rom"
        print types[t] "_package_rom:"           >> out
        print "  stage: " types[t]"_package"     >> out
        print "  tags: [ cross ]"                >> out
        print "  before_script: []"              >> out
        print "  script: echo"                   >> out
        print "  dependencies: []"               >> out
        print "  artifacts:"                     >> out
        print "    paths:"                       >> out
        print "      - RiscOS/Images/a*"         >> out
        print ""                                 >> out
      }
      # Complete build trees are big, and there's little utility in packaging up the snapshot build tree, so skip it
      if (types[t] == "latest") {
        print types[t] "_package_tree:"          >> out
        print "  stage: "types[t] "_package"     >> out
        print "  tags: [ cross ]"                >> out
        print "  before_script: []"              >> out
        print "  script: echo"                   >> out
        print "  dependencies: []"               >> out
        print "  artifacts:"                     >> out
        print "    paths:"                       >> out
        print "      - BuildInfo"                >> out
        print "      - RiscOS/*"                 >> out
        print ""                                 >> out
      }
    }
    # No point in filling up the runner's disc space with temporary files
    print "cleanup:"                                   >> out
    print "  stage: cleanup"                           >> out
    print "  tags: [ cross ]"                          >> out
    print "  before_script: []"                        >> out
    print "  script:"                                  >> out
    print "  - git submodule foreach 'git clean -xdf'" >> out
    print "  dependencies: []"                         >> out
    close(out)
  }
}

BEGIN {
  # Sort arrays alphabetically
  PROCINFO["sorted_in"] = "@ind_str_asc"

  # The list of builds is defined here statically. Maintain as necesssary.
  disc_build_list = "BuildHost Disc"
  rom_build_list  = "BCM2835 iMx6 IOMD32 OMAP3 OMAP4 OMAP5 PineA64 Titanium Tungsten"
  builds["disc"][0] = 0 # force awk to see these as two-dimensional arrays
  builds["rom"][0] = 0
  split(disc_build_list, builds["disc"])
  split(rom_build_list, builds["rom"])

  # Parse the ModuleDB
  print "Parsing ModuleDB..."
  cmd = "sed -e 's/#.*//' BuildSys/ModuleDB"
  while ((cmd | getline) > 0) {
    if (NF >= 3) {
      components[$1]["type"] = $2
      components[$1]["path"] = "RiscOS/" gensub(/[.]/, "/", "g", $3)
      if ($5 == "")
        components[$1]["target"] = $1
      else
        components[$1]["target"] = $5
    }
  }
  close(cmd)

  # Parse the 'All' superproject to establish a list of projects.
  print "Searching for projects..."
  while ((getline < "All/.gitmodules") > 0) {
    if ($1 == "path")
      path = $3
    if ($1 == "url") {
      project = gensub(/.*\//, "", "g", $3)
      project = gensub(/[.]git$/, "", "1", project)
      projects[project]["path"] = path
      projects[project]["builds"]["disc"][0] = 0
      delete projects[project]["builds"]["disc"][0]
      projects[project]["builds"]["rom"][0] = 0
      delete projects[project]["builds"]["rom"][0]
    }
  }
  close("All/.gitmodules")

  # Fetch a list of new projects that don't exist in 'All' yet from the command line
  # (This is so that we can bootstrap new project imports, that we want to test with
  # CI prior to import. You will need to manually add new components to the ModuleDB
  # as well - though you can do that in the local BuildSys submodule without even
  # committing it if you prefer.) For example:
  # ./refresh.awk RiscOS/Sources/Desktop/WindowScroll
  # Assume last element of path and URL match for these projects, for simplicity.
  for (arg = 1; arg < ARGC; arg++) {
    project = gensub(/.*\//, "", "g", ARGV[arg])
    print "Generating extra YAML for new project '" project "'..."
    projects[project]["path"] = ARGV[arg]
    projects[project]["builds"]["disc"][0] = 0
    delete projects[project]["builds"]["disc"][0]
    projects[project]["builds"]["rom"][0] = 0
    delete projects[project]["builds"]["rom"][0]
  }
  
  # Cross-reference components and projects by path. (Note this is a
  # many-to-one relationship.)
  print "Cross-referencing components and projects..."
  for (project in projects) {
    for (component in components) {
      if (components[component]["path"] == projects[project]["path"] ||
          substr(components[component]["path"], 1, length(projects[project]["path"]) + 1) == projects[project]["path"] "/") {
        components[component]["project"] = project
        projects[project]["component"][projects[project]["ncomponents"]++] = component
        if (components[component]["type"] == "ASM" ||
            components[component]["type"] == "BAS" ||
            components[component]["type"] == "C")
          projects[project]["softloadable"] = "yes"
      }
    }
  }

  # Parse the components file to determine which projects affect which builds.
  print "Parsing component files..."
  find_project_builds("disc")
  find_project_builds("rom")

  # Remove any old YAML files
  system("rm -f CI/*.yml")

  # Regenerate project YAML files
  print "Generating project YAML files..."
  for (project in projects) {
    out = "CI/" project ".yml"

    print "workflow:"                       >> out
    print "  rules:"                        >> out
    print "    - if: $CI_MERGE_REQUEST_IID" >> out
    print "    - if: $CI_COMMIT_TAG"        >> out
    print "    - if: $CI_COMMIT_BRANCH"     >> out
    print ""                                >> out

    # Define appropriate stages for this project
    print "stages:" >> out
    print "  - static_analysis" >> out
    if (projects[project]["softloadable"] == "yes")
      print "  - softload" >> out
    if (projects[project]["buildable"]["disc"] == "yes")
      print "  - disc" >> out
    if (projects[project]["buildable"]["rom"] == "yes")
      print "  - rom" >> out
    print "  - deploy" >> out
    print "  - cleanup" >> out
    print "" >> out

    # Write static analysis rules. There are always at least the first nine.
    print "gitattributes:"                                                                   >> out
    print "  stage: static_analysis"                                                         >> out
    print "  tags: [ cross ]"                                                                >> out
    check_suppress("gitattributes")
    print "  script:"                                                                        >> out
    print "    - >"                                                                          >> out
    print "        function top_dir {"                                                       >> out
    print "        if [ -d \"$1\" ] && ! grep -q \"^$1/\\*\\* \" .gitattributes; then"       >> out
    print "        echo \".gitattributes lacks pattern for top-level $1 directory\"; false;" >> out
    print "        fi"                                                                       >> out
    print "        };"                                                                       >> out
    print "        function nested_dir {"                                                    >> out
    print "        if [ $(find . -mindepth 2 -type d -name \"$1\" | wc -c) -ne 0 ] &&"       >> out
    print "        ! grep -q \"^\\*\\*/$1/\\*\\* \" .gitattributes; then"                    >> out
    print "        echo \".gitattributes lacks pattern for nested $1 directory\"; false;"    >> out
    print "        fi"                                                                       >> out
    print "        };"                                                                       >> out
    print "        function dir_type {"                                                      >> out
    print "        top_dir \"$1\";"                                                          >> out
    print "        nested_dir \"$1\";"                                                       >> out
    print "        };"                                                                       >> out
    print "        function file_type {"                                                     >> out
    print "        if [ $(find . -type f -name \"*,$1\" | wc -c) -ne 0 ] &&"                 >> out
    print "        ! grep -q \"^\\*,$1 \" .gitattributes; then"                              >> out
    print "        echo \".gitattributes lacks pattern for filetype $1\"; false;"            >> out
    print "        fi"                                                                       >> out
    print "        };"                                                                       >> out
    print "        dir_type  Hdr;"                                                           >> out
    print "        dir_type  hdr;"                                                           >> out
    print "        dir_type  s;"                                                             >> out
    print "        dir_type  awk;"                                                           >> out
    print "        dir_type  bas;"                                                           >> out
    print "        file_type ffb;"                                                           >> out
    print "        file_type fd1;"                                                           >> out
    print "        dir_type  c;"                                                             >> out
    print "        dir_type  h;"                                                             >> out
    print "        dir_type  x;"                                                             >> out
    print "        dir_type  CMHG;"                                                          >> out
    print "        dir_type  cmhg;"                                                          >> out
    print "        dir_type  c++;"                                                           >> out
    print "        file_type fe1;"                                                           >> out
    print "        dir_type  pl;"                                                            >> out
    print "        file_type 102;"                                                           >> out
    print "  allow_failure: true"                                                            >> out
    print ""                                                                                 >> out

    print "gitignore:"                        >> out
    print "  stage: static_analysis"          >> out
    print "  tags: [ cross ]"                 >> out
    check_suppress("gitignore")
    print "  script:"                         >> out
    print "    - test -f .gitignore"          >> out
    print "  allow_failure: true"             >> out
    print ""                                  >> out

    print "license:"                          >> out
    print "  stage: static_analysis"          >> out
    print "  tags: [ cross ]"                 >> out
    check_suppress("license")
    print "  script:"                         >> out
    print "    - >"                           >> out
    print "        test -f LICENSE ||"        >> out
    print "        test -f License.txt ||"    >> out
    print "        test -f LICENCE ||"        >> out
    print "        test -f Licence ||"        >> out
    print "        test -f COPYING ||"        >> out
    print "        test -f Copying ||"        >> out
    print "        test -f COPYINGLIB ||"     >> out
    print "        test -f COPYING.LESSER ||" >> out
    print "        test -f Artistic"          >> out
    print "  allow_failure: true"             >> out
    print ""                                  >> out

    print "versionnum:"                       >> out
    print "  stage: static_analysis"          >> out
    print "  tags: [ cross ]"                 >> out
    check_suppress("versionnum")
    print "  script:"                         >> out
    print "    - test -f VersionNum"          >> out
    print "  allow_failure: true"             >> out
    print ""                                  >> out

    print "head_log:"                                                                                                                 >> out
    print "  stage: static_analysis"                                                                                                  >> out
    print "  tags: [ cross ]"                                                                                                         >> out
    check_suppress("head_log")
    print "  script:"                                                                                                                 >> out
    print "    - >"                                                                                                                   >> out
    print "        git log -1 --pretty=format:%B | awk '"                                                                             >> out
    print "        NR==1 && length($0)>70 { print \"Commit log summary line exceeds 70 characters\"; result=1 }"                      >> out
    print "        NR==2 && length($0)>0  { print \"Commit log requires a single-line summary followed by a blank line\"; result=1 }" >> out
    print "        NR>=2 && length($0)>80 { print \"Commit log line \"NR\" exceeds 80 characters\"; result=1 }"                       >> out
    print "        END { exit result }'"                                                                                              >> out
    print "  allow_failure: true"                                                                                                     >> out
    print ""                                                                                                                          >> out

    print "merge_log:"                                                                                                                                                                              >> out
    print "  stage: static_analysis"                                                                                                                                                                >> out
    print "  tags: [ cross ]"                                                                                                                                                                       >> out
    print "  rules:"                                                                                                                                                                                >> out
    print "    - if: '$CI_MERGE_REQUEST_ID && $SUPPRESS_JOBS !~ /\\bmerge_log\\b/'"                                                                                                                 >> out
    print "  script:"                                                                                                                                                                               >> out
    print "    - >"                                                                                                                                                                                 >> out
    print "        git remote show | grep target > /dev/null && git remote remove target || true;"                                                                                                  >> out
    print "        git remote add -t $CI_MERGE_REQUEST_TARGET_BRANCH_NAME -f target $CI_MERGE_REQUEST_PROJECT_URL.git;"                                                                             >> out
    print "        has_versionnum=1; custom_versionnum=0;"                                                                                                                                          >> out
    print "        if test -f VersionNum; then grep -q \"This file is automatically maintained by srccommit, do not edit manually.\" VersionNum || custom_versionnum=1; else has_versionnum=0; fi;" >> out 
    print "        h=$(git rev-parse HEAD);"                                                                                                                                                        >> out
    print "        for r in $(git rev-list --reverse $(git merge-base HEAD target/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME)..HEAD);"                                                                    >> out
    print "        do"                                                                                                                                                                              >> out
    print "        git log -1 --pretty=format:%B $r | awk '"                                                                                                                                        >> out
    print "        BEGIN { keywords=0; nochange=0; notag=0; tag=\"\"; printf(\"\\033[91m\") }"                                                                                                      >> out
    print "        /!NoChange|!NoTag|!Tag\\([^)]+\\)/ { ++keywords }"                                                                                                                               >> out
    print "        /!NoChange/ { if (\"'$r'\"==\"'$h'\") nochange=1; else { print \"Commit '$r' is not tip of merge request and uses !NoChange keyword in log\"; result=1 } }"                      >> out
    print "        /!NoTag/ { if (\"'$r'\"==\"'$h'\") notag=1; else { print \"Commit '$r' is not tip of merge request and uses !NoTag keyword in log\"; result=1 } }"                               >> out
    print "        /!Tag\\([^)]+\\)/ { if (\"'$r'\"==\"'$h'\") tag=gensub(/^.*!Tag\\(([^)]+)\\).*$/, \"\\\\1\", \"g\");"                                                                            >> out
    print "        else { print \"Commit '$r' is not tip of merge request and uses !Tag keyword in log\"; result=1 } }"                                                                             >> out
    print "        NR==1 && length($0)>70 { print \"Commit '$r' log summary line exceeds 70 characters\"; result=1 }"                                                                               >> out
    print "        NR==2 && length($0)>0  { print \"Commit '$r' log requires a single-line summary followed by a blank line\"; result=1 }"                                                          >> out
    print "        NR>=2 && length($0)>80 { print \"Commit '$r' log line \"NR\" exceeds 80 characters\"; result=1 }"                                                                                >> out
    print "        END {"                                                                                                                                                                           >> out
    print "        if (keywords > 1) { print \"Commit '$r' uses multiple keywords in log\"; result = 1 }"                                                                                           >> out
    print "        if (\"'$r'\"==\"'$h'\") {"                                                                                                                                                       >> out
    print "        if (!'$has_versionnum' && tag==\"\") { print \"Either create a VersionNum file or use !Tag keyword in log\"; result=1 }"                                                         >> out
    print "        if ('$custom_versionnum' && tag==\"\" && notag==0) { print \"Non-standard VersionNum file requires either !Tag or !NoTag keyword in log\"; result=1 }"                           >> out
    print "        if (tag!=\"\") {"                                                                                                                                                                >> out
    print "        if ('$has_versionnum' && !'$custom_versionnum') { print \"!Tag keyword must not be used with standard VersionNum file\"; result=1 }"                                             >> out
    print "        if (system(\"git check-ref-format \\\"tags/\" tag \"\\\"\")) { print \"Invalid tag name\"; result = 1 }"                                                                         >> out
    print "        else if (!system(\"git ls-remote --exit-code --tags target \" tag \" > /dev/null\")) { print \"Tag already exists in destination repository\"; result=1 }"                       >> out
    print "        }"                                                                                                                                                                               >> out
    print "        printf(\"\\033[39m\")"                                                                                                                                                           >> out
    print "        }"                                                                                                                                                                               >> out
    print "        exit result;"                                                                                                                                                                    >> out
    print "        }';"                                                                                                                                                                             >> out
    print "        done"                                                                                                                                                                            >> out
    print "  allow_failure: true"                                                                                                                                                                   >> out
    print ""                                                                                                                                                                                        >> out

    print "makefile:"                                                                                                 >> out
    print "  stage: static_analysis"                                                                                  >> out
    print "  tags: [ cross ]"                                                                                         >> out
    check_suppress("makefile")
    print "  script:"                                                                                                 >> out
    print "    - >"                                                                                                   >> out
    print "        for f in $(find *"                                                                                 >> out
    print "        -name \"makefile\" -o"                                                                             >> out
    print "        -name \"*Make*\" -o"                                                                               >> out
    print "        -name \"*.mk\" -o"                                                                                 >> out
    print "        -name \"*,fe1\" -o"                                                                                >> out
    print "        -name \"AutoGenMfS\"); do awk '"                                                                   >> out
    print "        ''STATE==2                    { print \"'$f' contains unstripped dynamic dependencies\"; exit 1 }" >> out
    print "        ''STATE==0 && /^ /            { print \"'$f' line \"NR\" should start with a tab character\" }"    >> out
    print "        ''                            { STATE=0 }"                                                         >> out
    print "        ''/\\\\$/                       { STATE=1 }"                                                       >> out
    print "        ''/^# Dynamic dependencies:$/ { STATE=2 }"                                                         >> out
    print "        ' $f; done"                                                                                        >> out
    print "  allow_failure: true"                                                                                     >> out
    print ""                                                                                                          >> out

    print "head_whitesp:"                                                        >> out
    print "  stage: static_analysis"                                             >> out
    print "  tags: [ cross ]"                                                    >> out
    check_suppress("head_whitesp")
    print "  script:"                                                            >> out
    print "    - >"                                                              >> out
    print "        status=0;"                                                    >> out
    print "        exceptions=();"                                               >> out
    print "        while read -d \" \" part; do"                                 >> out
    print "        if [ \"$part\" != \"\" ]; then"                               >> out
    print "        exceptions=(\"${exceptions[@]}\" -path \"$part\" -prune -o);" >> out
    print "        fi;"                                                          >> out
    print "        done <<< \"$WHITESPACE_WHITELIST \";"                         >> out
    print "        find . \"${exceptions[@]}\""                                  >> out
    print "        -path \"./.git\" -prune -o"                                   >> out
    print "        -path \"./Resources*/Messages\" -prune -o"                    >> out
    print "        -type f -print |"                                             >> out
    print "        xargs grep -In $'\\(^  *\\t\\|[\\t ]$\\)'"                    >> out
    print "        || status=1; test $status != 0"                               >> out
    print "  allow_failure: true"                                                >> out
    print ""                                                                     >> out

    print "merge_whitesp:"                                                                                              >> out
    print "  stage: static_analysis"                                                                                    >> out
    print "  tags: [ cross ]"                                                                                           >> out
    print "  rules:"                                                                                                    >> out
    print "    - if: '$CI_MERGE_REQUEST_ID && $SUPPRESS_JOBS !~ /\\bmerge_whitesp\\b/'"                                 >> out
    print "  script:"                                                                                                   >> out
    print "    - >"                                                                                                     >> out
    print "        git remote show | grep target > /dev/null && git remote remove target || true;"                      >> out
    print "        git remote add -t $CI_MERGE_REQUEST_TARGET_BRANCH_NAME -f target $CI_MERGE_REQUEST_PROJECT_URL.git;" >> out
    print "        awk '"                                                                                               >> out
    print "        ''BEGIN {"                                                                                           >> out
    print "        ''  result = 0;"                                                                                     >> out
    print "        ''  cmd = \"git diff -b -U0 target/'$CI_MERGE_REQUEST_TARGET_BRANCH_NAME' HEAD\";"                   >> out
    print "        ''  while ((cmd | getline) > 0) {"                                                                   >> out
    print "        ''    if ($1 == \"+++\")"                                                                            >> out
    print "        ''      file = gensub(/^b\\//, \"\", \"1\", $2);"                                                    >> out
    print "        ''    else if ($1 == \"@@\")"                                                                        >> out
    print "        ''      line = gensub(/+([0-9]+).*/, \"\\\\1\", \"1\", $3);"                                         >> out
    print "        ''    else if ($0 ~ /^+/) {"                                                                         >> out
    print "        ''      for (nearline = line-20; nearline <= line+20; ++nearline)"                                   >> out
    print "        ''        change[file][nearline];"                                                                   >> out
    print "        ''      ++line;"                                                                                     >> out
    print "        ''    }"                                                                                             >> out
    print "        ''  }"                                                                                               >> out
    print "        ''  close(cmd);"                                                                                     >> out
    print "        ''  cmd = \"git diff -U0 target/'$CI_MERGE_REQUEST_TARGET_BRANCH_NAME' HEAD\";"                      >> out
    print "        ''  while ((cmd | getline) > 0) {"                                                                   >> out
    print "        ''    if ($1 == \"+++\") {"                                                                          >> out
    print "        ''      file = gensub(/^b\\//, \"\", \"1\", $2);"                                                    >> out
    print "        ''      change[file][\"dummy\"]"                                                                     >> out
    print "        ''    } else if ($1 == \"@@\") {"                                                                    >> out
    print "        ''      line = gensub(/+([0-9]+).*/, \"\\\\1\", \"1\", $3);"                                         >> out
    print "        ''      had_error = 0;"                                                                              >> out
    print "        ''    } else if ($0 ~ /^-/) {"                                                                       >> out
    print "        ''      if ($0 ~ /( +\\t|[\\t ]$)/)"                                                                 >> out
    print "        ''        had_error = 1;"                                                                            >> out
    print "        ''    } else if ($0 ~ /^+/) {"                                                                       >> out
    print "        ''      if (had_error && !(line in change[file])) {"                                                 >> out
    print "        ''        print file \" line \" line \" probably removes whitespace error\";"                        >> out
    print "        ''        ++result;"                                                                                 >> out
    print "        ''      }"                                                                                           >> out
    print "        ''      ++line;"                                                                                     >> out
    print "        ''    }"                                                                                             >> out
    print "        ''  }"                                                                                               >> out
    print "        ''  close(cmd)"                                                                                      >> out
    print "        ''}"                                                                                                 >> out
    print "        ''END { exit result >= 10 }'"                                                                        >> out
    print "  allow_failure: true"                                                                                       >> out
    print ""                                                                                                            >> out

    print "copyright:"                                                                          >> out
    print "  stage: static_analysis"                                                            >> out
    print "  tags: [ cross ]"                                                                   >> out
    check_suppress("copyright")
    print "  script:"                                                                           >> out
    print "    - >"                                                                             >> out
    print "        status=0;"                                                                   >> out
    print "        exceptions=();"                                                              >> out
    print "        while read -d \" \" part; do"                                                >> out
    print "        if [ \"$part\" != \"\" ]; then"                                              >> out
    print "        exceptions=(\"${exceptions[@]}\" -path \"$part\" -prune -o);"                >> out
    print "        fi;"                                                                         >> out
    print "        done <<< \"$COPYRIGHT_WHITELIST ./.git ./Resources \";"                      >> out
    print "        for f in $(find . \"${exceptions[@]}\" -type f -print |"                     >> out
    print "        grep -E '(/(hdr|s|awk|bas|c|h|x|cmhg|c\\+\\+|pl)/|,ffb$|,fd1$|,102$)'); do"  >> out
    print "        grep -q -w -E '(Copyright|SPDX-FileCopyrightText)' $f || echo $f; done |"    >> out
    print "        tee >( wc -l ) 1>&2 | { read lines; exit $lines; }"                          >> out
    print "  allow_failure: true"                                                               >> out
    print ""                                                                                    >> out

    likely_c = "no"
    for (i = 0; i < projects[project]["ncomponents"]; ++i) {
      if (components[projects[project]["component"][i]]["type"] == "C" ||
          components[projects[project]["component"][i]]["type"] == "EXP")
        likely_c = "yes"
    }
    if (likely_c == "yes") {
      print "cppcheck:"                      >> out
      print "  stage: static_analysis"       >> out
      print "  tags: [ cross ]"              >> out
      check_suppress("cppcheck")
      print "  script:"                      >> out
      print "    - 'curl --location --output ~/cache/common/Disc.zip -z ~/cache/common/Disc.zip \"https://gitlab.riscosopen.org/Products/Disc/-/jobs/artifacts/master/download?job=latest_package_tree\"'" >> out
      print "    - unzip -q ~/cache/common/Disc.zip"                                                                  >> out
      print "    - source RiscOS/Env/ROOL/Disc.sh"                                                                    >> out
      print "    # Build the module in case we require that one or more autogenerated header files exist"             >> out
      for (i = 0; i < projects[project]["ncomponents"]; ++i) {
        component = projects[project]["component"][i]
        if (components[component]["path"] == projects[project]["path"])
          rel = ""
        else
          rel = " -C " substr(components[component]["path"], length(projects[project]["path"]) + 2)
        print "    - COMPONENT=" component " TARGET=" components[component]["target"] " INSTDIR=$INSTALLDIR make -k" rel " clean"               >> out
        print "    - COMPONENT=" component " TARGET=" components[component]["target"] " INSTDIR=$INSTALLDIR make -k" rel " export_hdrs || true" >> out
        print "    - COMPONENT=" component " TARGET=" components[component]["target"] " INSTDIR=$INSTALLDIR make -k" rel " export_libs || true" >> out
        print "    - COMPONENT=" component " TARGET=" components[component]["target"] " INSTDIR=$INSTALLDIR make -k" rel " standalone  || true" >> out
      }
      # Extract the C include path and defines directly from the makefile
      print "    - CFLAGS=$( { cd objs && { echo $'cflags:\\n\\t@echo ${CINCLUDES} ${CDEFINES}'; cat ../Makefile; } | make -f -; } || true )" >> out
      if (project == "SDIODriver")
        print "    - CPPCHECK_EXTRA=\"--suppress=knownConditionTrueFalse:objs/swi.c --suppress=nullPointerRedundantCheck:objs/device.c\"" >> out
      # Enable all warnings except those marked inline, plus shiftNegativeLHS (used in _swix macros and
      # therefore ubiquitous) and purgedConfiguration (we don't care if a build switch has no effect).
      # cppcheck's --error-exitcode option appears to be too eager to report failure, so synthesise it by
      # counting the number of lines printed to stderr.
      print "    - test ! -d c || { { { { cppcheck --enable=all --suppress=shiftNegativeLHS --suppress=purgedConfiguration --suppress=unusedFunction --suppress=variableScope --inline-suppr $CPPCHECK_EXTRA --std=c99 $CFLAGS -UNAMESPACE_OSLIB -U__swi --verbose --max-configs=100 objs/*.c; } 3>&1 1>&2 2>&3; } | tee >( grep -v '^[^ ]* information[:] ' | grep -v '^$' | wc -l >&3 ); } 3>&1 1>&2; } | { read lines; exit $lines; }" >> out
      print "  allow_failure: true"          >> out
      print ""                               >> out
    }

    # Write rules for softload targets.
    if (projects[project]["softloadable"] == "yes") {
      write_softload_rules("")
      write_softload_rules("_gnu")
    }

    # Write rules for disc and ROM builds.
    #write_build_rules("disc")
    #write_build_rules("rom")

    # Write cleanup rule - no point in filling up the runner's disc space with temporary files
    print "cleanup:"                         >> out
    print "  stage: cleanup"                 >> out
    print "  tags: [ cross ]"                >> out
    print "  script:"                        >> out
    print "  - git clean -xdf"               >> out

    close(out)
  }

  # Regenerate superproject YAML files
  print "Generating superproject YAML files..."
  do_superproject_class("disc")
  do_superproject_class("rom")
}

